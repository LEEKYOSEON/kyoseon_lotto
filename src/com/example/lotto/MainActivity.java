package com.example.lotto;

//import android.support.v7.app.ActionBarActivity;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import com.kyoseon.lotto.R;

public class MainActivity extends Activity implements OnClickListener {

	private Button button;
	private TextView num1;
	private TextView num2;
	private TextView num3;
	private TextView num4;
	private TextView num5;
	private TextView num6;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		button = (Button) findViewById(R.id.ButtonGo);
		num1 = (TextView) findViewById(R.id.Text1);
		num2 = (TextView) findViewById(R.id.Text2);
		num3 = (TextView) findViewById(R.id.Text3);
		num4 = (TextView) findViewById(R.id.Text4);
		num5 = (TextView) findViewById(R.id.Text5);
		num6 = (TextView) findViewById(R.id.Text6);
		button.setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()) {
		case R.id.ButtonGo:
			getLottoNum();
			break;
		}
	}
	
	private void getLottoNum() {
		int[] numArray = new int[45];
		int temp = 0;
				
		for(int i = 0; i < 45 ; i++) {
			numArray[i] = i+1;
		}
		
		for(int j = 0; j < 100; j++) {
			int a = (int) (Math.random() * 45);
			temp = numArray[0];
			numArray[0] = numArray[a];
			numArray[a] = temp;			
		}
		
		num1.setText(String.valueOf(numArray[0]));
		num2.setText(String.valueOf(numArray[1]));
		num3.setText(String.valueOf(numArray[2]));
		num4.setText(String.valueOf(numArray[3]));
		num5.setText(String.valueOf(numArray[4]));
		num6.setText(String.valueOf(numArray[5]));
	}
}
